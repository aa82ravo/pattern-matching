package edu.leipzig.streaming.graphs.patternmatching.model;

import org.apache.flink.api.java.tuple.Tuple3;

public
class StreamObject<V extends StreamVertex, E extends StreamEdge> extends Tuple3<E, V, V> {

    /**
     * Default constructor is necessary to apply to POJO rules.
     */
    public StreamObject() {
    }

    /**
     * constructor
     */
    /*public StreamObject(String id, long timestamp, String label,
                        V source, V target) {
        this.f0 = id;
        this.f1 = timestamp;
        this.f2 = label;
        this.f3 = Properties.create();
        this.f4 = source;
        this.f5 = target;
    }*/
    public StreamObject(E edge, V source, V target) {
        this.f0 = edge;
        this.f1 = source;
        this.f2 = target;
    }

    /**
     * constructor with all fields
     */
    /*public StreamObject(String id, long timestamp, String label, Properties properties,
                        V source, V target) {
        this.f0 = id;
        this.f1 = timestamp;
        this.f2 = label;
        this.f3 = properties;
        this.f4 = source;
        this.f5 = target;
    }*/
    public String getEdgeLabel() {
        return this.f0.getLabel();
    }

    public String getSourceId() {
        return this.f1.getId();
    }

    public long getTimestamp() {
        return this.f0.getTimestamp();
    }

    public V getTarget() {
        return this.f2;
    }

    public void setTarget(V target) {
        this.f2 = target;
    }

    public V getSource() {
        return this.f1;
    }

    public void setSource(V source) {
        this.f1 = source;
    }

    public E getEdge() {
        //return (E) E.create(this.f0, this.f1, this.f2, this.f3, this.f4.id, this.f5.id, this.f6);
        return this.f0;
    }

    public void setEdge(E edge) {
        if (edge != null) {
            this.f0 = edge;
        }
            /*this.f0 = edge.id;
            this.f1 = edge.getTimestamp();
            this.f2 = edge.getLabel();
            this.f3 = edge.getProperties();
            this.f6 = edge.getVariable();
        }*/
    }


    @Override
    public String toString() {
        /*return "(" + f4 + ")" + " -[ " +
                f0 + "," +
                f1 + "," +
                f2 + "," +
                f3 + " ] ->" + "(" + f5 + ")";*/
        return "(" + f1 + ")" + " -[ " +
                f0.getId() + "," +
                f0.getTimestamp() + "," +
                f0.getLabel() + "," +
                f0.getProperties() + " ] ->" + "(" + f2 + ")";
        /*return "E:" +
                f0 + "," +
                f1 + "," +
                f2 + "," +
                f3 +
                ";V:" + f4 +
                ";V:" + f5;*/
    }

    @Override
    public boolean equals(Object o) {
        if (getClass() != o.getClass()) {
            return false;
        }

        if (this == o) {
            return true;
        }

        StreamObject<V, E> streamObject = (StreamObject<V, E>) o;
        /*return this.getSource().innerMatch(streamObject.getSource()) &&
                this.getTarget().innerMatch(streamObject.getTarget()) &&
                this.getEdge().innerMatch(streamObject.getEdge());*/
        return this.getSource().innerMatch(streamObject.getSource()) &&
                this.getTarget().innerMatch(streamObject.getTarget()) &&
                this.f0.innerMatch(streamObject.f0);
    }
}
