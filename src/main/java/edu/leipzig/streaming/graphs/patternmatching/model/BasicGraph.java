package edu.leipzig.streaming.graphs.patternmatching.model;

import org.apache.flink.streaming.api.windowing.windows.TimeWindow;

import java.io.Serializable;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.*;

public class BasicGraph<E extends StreamEdge, V extends StreamVertex> implements Serializable {
    final ArrayList<E> edges;
    final ArrayList<V> vertices;
    private String windowStart;
    private String windowEnd;

    public BasicGraph() {
        edges = new ArrayList<>();
        vertices = new ArrayList<>();
    }

    public BasicGraph(Collection<E> edges, Collection<V> vertices, TimeWindow w) {
        this.edges = new ArrayList<>(edges);
        this.vertices = new ArrayList<>(vertices);
        this.windowStart = convertTime(w.getStart());
        this.windowEnd = convertTime(w.getEnd());
    }


    private String convertTime(long time) {
        Date date = new Date(time);
        Format format = new SimpleDateFormat("yyyy MM dd HH:mm:ss");
        return format.format(date);
    }

    public ArrayList<E> getEdges() {
        return edges;
    }

    public ArrayList<V> getVertices() {
        return vertices;
    }

    public void addEdge(E e) {
        edges.add(e);
    }

    public void addVertex(V v) {
        vertices.add(v);
    }

    public <E extends Element> Collection<E> getElements() {
        Collection<E> e = (Collection<E>) this.edges;
        Collection<E> v = (Collection<E>) this.vertices;
        e.addAll(v);
        return e;
    }

    public <O extends StreamObject<QueryVertex, QueryEdge>> Collection<O> getTriples() {
        ArrayList<StreamObject<QueryVertex, QueryEdge>> result = new ArrayList<>();
        for (E e : edges) {
            StreamObject triple = new StreamObject();
            Optional<V> source = vertices.stream().filter(element -> element.getId().equals(e.getSourceId())).findFirst();
            source.ifPresent(triple::setSource);
            Optional<V> target = vertices.stream().filter(element -> element.getId().equals(e.getTargetId())).findFirst();
            target.ifPresent(triple::setTarget);
            triple.setEdge(e);
            result.add(triple);
        }
        return (Collection<O>) result;
    }

    @Override
    public String toString() {
        if (edges.size() > 0 && vertices.size() > 0) {
            return String.format("-----------------------------------\ncount=%s \nTimeWindow{start=%s, end=%s}\n%s", this.getTriples().size(), windowStart, windowEnd, Arrays.toString(this.getTriples().toArray()));
        } else {
            return String.format("-----------------------------------\ncount=%s \nTimeWindow{start=%s, end=%s}\n%s", this.vertices.size(), windowStart, windowEnd, Arrays.toString(this.vertices.toArray()));
        }
    }

    public String getJSONString() {
        if (edges.size() > 0) {
            StringBuilder points = new StringBuilder("[");
            for (V v : this.vertices) {
                points.append(getVertexJSONString(v));
                points.append(",");
            }
            points = new StringBuilder(points.substring(0, points.length() - 1));
            points.append("]");
            StringBuilder edges = new StringBuilder("[");
            for (E e : this.edges) {
                edges.append(getEdgeJSONString(e));
                edges.append(",");
            }
            edges = new StringBuilder(edges.substring(0, edges.length() - 1));
            edges.append("]");
            StringBuilder categories = new StringBuilder("[");
            ArrayList<Category> Categories = new ArrayList<Category>();
            Categories.add(new Category("Developer", "10", "#CC0000"));
            Categories.add(new Category("Company", "50", "#E79F13"));
            Categories.add(new Category("Project", "25", "#45818E"));
            for (Category entry : Categories) {
                categories.append(entry.toString());
                categories.append(",");
            }
            categories = new StringBuilder(categories.substring(0, categories.length() - 1));
            categories.append("]");
            return String.format("{\"points\":%s, \"edges\":%s, \"version\":\"%s\", \"categories\":%s}",
                    points.toString(), edges.toString(), "start", categories.toString());
        } else
            return "";
    }

    private String getEdgeJSONString(E e) {
        return String.
                format("{\"source\":\"%s\", \"target\":\"%s\"," +
                                " \"value\":\"%s\"," +
                                " \"label\": {\"normal\":{\"show\": true}}," +
                                " \"lineStyle\": {\"normal\":{\"width\":%d}}}"
                        , e.getSourceId(), e.getTargetId(), e.getLabel() /*+ " | " + value.toString()*/, 1);
    }

    private String getVertexJSONString(V v) {
        return String.format("{\"name\":\"%s\",\"symbolSize\":\"%d\" ,\"category\":\"%s\",\"value\":\"%s\"," +
                        "\"label\": {\"normal\":{\"show\": true}}}",
                v.getId(), 25, v.getLabel(), v.properties.get("id") /*+ " | " + str*/);
    }
}

class Category {
    private String name;
    private String symbolSize;
    private String color;

    public Category(String name, String symbolSize, String color) {
        this.name = name;
        this.symbolSize = symbolSize;
        this.color = color;
    }

    @Override
    public String toString() {
        return String.
                format("{\"name\":\"%s\", \"itemStyle\":{\"color\": \"%s\"}}"
                        , name, color);
    }
}

