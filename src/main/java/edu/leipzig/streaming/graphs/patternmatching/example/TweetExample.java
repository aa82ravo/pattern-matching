package edu.leipzig.streaming.graphs.patternmatching.example;

import edu.leipzig.streaming.graphs.patternmatching.PatternMatcher;
import edu.leipzig.streaming.graphs.patternmatching.model.*;
import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.metrics.Counter;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;
import org.apache.flink.streaming.api.windowing.assigners.SlidingProcessingTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.connectors.twitter.TwitterSource;
import org.apache.flink.util.Collector;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.properties.Properties;

import java.util.ArrayList;
import java.util.Arrays;

public class TweetExample {

    public static void main(String[] args) throws Exception {

        // set up the streaming execution environment
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setStreamTimeCharacteristic(TimeCharacteristic.ProcessingTime);

        // Checking input parameters
        final ParameterTool params = ParameterTool.fromArgs(args);
        System.out.println("Usage: TwitterExample [--output <path>] " +
                "[--twitter-source.consumerKey <key> --twitter-source.consumerSecret <secret> --twitter-source.token " +
                "<token> --twitter-source.tokenSecret <tokenSecret>]");

        // make parameters available in the web interface
        env.getConfig().setGlobalJobParameters(params);

        DataStream<String> streamSource;
        if (params.has(TwitterSource.CONSUMER_KEY) && params.has(TwitterSource.CONSUMER_SECRET) &&
                params.has(TwitterSource.TOKEN) && params.has(TwitterSource.TOKEN_SECRET)) {
            streamSource = env.addSource(new TwitterSource(params.getProperties()));
        } else {
            System.out.println("Executing TwitterStream example with default props.");
            System.out.println("Use --twitter-source.consumerKey <key> --twitter-source.consumerSecret <secret> " +
                    "--twitter-source.token <token> --twitter-source.tokenSecret <tokenSecret> specify the " +
                    "authentication info.");
            return;
        }

        DataStream<StreamObject<StreamVertex, StreamEdge>> edgeStream =
                streamSource.flatMap(new RichFlatMapFunction<String, StreamObject<StreamVertex, StreamEdge>>() {
                    private static final long serialVersionUID = 1L;
                    private transient ObjectMapper jsonParser;
                    private transient Counter counter;

                    @Override
                    public void open(Configuration config) {
                        this.counter = getRuntimeContext()
                                .getMetricGroup()
                                .counter("newTweets");
                    }

                    @Override
                    public void flatMap(String value, Collector<StreamObject<StreamVertex, StreamEdge>> collector) throws
                            Exception {

                        if (jsonParser == null) {
                            jsonParser = new ObjectMapper();
                        }

                        JsonNode jsonNode = jsonParser.readValue(value, JsonNode.class);

                        boolean isEnglish = jsonNode.has("lang") && jsonNode.get("lang").asText().equals("en");
                        boolean hasText = jsonNode.has("text");
                        if (isEnglish/* && hasText*/) {
                            Long timestamp = jsonNode.get("timestamp_ms").asLong();
                            Properties tweetProps = Properties.create();
                            tweetProps.set("id", jsonNode.get("id_str").asText());
                            tweetProps.set("lang", jsonNode.get("lang").asText());
                            tweetProps.set("reply_count", jsonNode.get("reply_count").asLong());
                            tweetProps.set("retweet_count", jsonNode.get("retweet_count").asLong());
                            if (jsonNode.get("retweet_count").asLong() > 0)
                                System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>0");
                            tweetProps.set("favorite_count", jsonNode.get("favorite_count").asLong());
                            tweetProps.set("reply_count", jsonNode.get("reply_count").asText());
                            StreamVertex tweet = new StreamVertex(jsonNode.get("id_str").asText(), "Tweet", tweetProps);

                            JsonNode jsonUser = jsonNode.get("user");
                            Properties userProps = Properties.create();
                            userProps.set("id", jsonUser.get("id_str").asText());
                            userProps.set("followers_count", jsonUser.get("followers_count").asLong());
                            userProps.set("following", jsonUser.get("following").asBoolean());
                            userProps.set("location", jsonUser.get("location").asText());
                            StreamVertex user = new StreamVertex(jsonUser.get("id_str").asText(), "User", userProps);
                            //String id, Long timestamp, String label, Properties properties, String sourceId, String targetId
                            StreamEdge creator = new StreamEdge(GradoopId.get().toString(), timestamp, "creator", null,
                                    tweet.getId(), user.getId());
                            this.counter.inc();
                            collector.collect(new StreamObject<>(creator, tweet, user));
                        }

                    }
                });

        String query = "MATCH (a:Tweet)-[b:creator]->(c:User) WHERE c.followers_count > 1000L AND (a.retweet_count > 1000 OR a.location =\"Naga City\")";
        String query2 = "MATCH (a:Tweet)-[b:creator]->(c:User) WHERE a.retweet_count > 0L";
        String query3 = "MATCH (a:Tweet)-[b:creator]->(c:User) WHERE c.location =\"Indonesia\"";

        String[] order = {"a", "b"};
        PatternMatcher matcher = new PatternMatcher(
                edgeStream,
                SlidingProcessingTimeWindows.of(Time.seconds(60), Time.seconds(5)),
                query3, false, new ArrayList<>(Arrays.asList(order)));

        PatternMatchResult result = matcher.execute();
        result.getResultStream().print();

        env.execute("Tweets");
    }
}

