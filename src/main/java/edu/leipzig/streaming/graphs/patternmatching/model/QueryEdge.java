package edu.leipzig.streaming.graphs.patternmatching.model;

import org.gradoop.common.model.impl.properties.Properties;
import org.gradoop.common.model.impl.properties.PropertyValue;
import org.s1ck.gdl.model.comparables.ComparableExpression;
import org.s1ck.gdl.model.comparables.Literal;
import org.s1ck.gdl.model.comparables.PropertySelector;
import org.s1ck.gdl.model.predicates.Predicate;
import org.s1ck.gdl.model.predicates.expressions.Comparison;

import java.util.ArrayList;
import java.util.Collection;

public class QueryEdge extends StreamEdge implements HasPredicate {
    Collection<Predicate> selfPredicates;
    int order;

    public QueryEdge(String id, Long timestamp, String label, Properties properties, String sourceId, String targetId, String variable) {
        super(id, timestamp, label, properties, sourceId, targetId, variable);
        selfPredicates = new ArrayList<>();
    }

    public String getVariable() {
        return this.variables.get(0);
    }

    QueryEdge() {
        selfPredicates = new ArrayList<>();
    }

    public static QueryEdge create(String id, Long timestamp, String label, Properties properties, String sourceId, String targetId, String variable) {
        return new QueryEdge(id, timestamp, label, properties, sourceId, targetId, variable);
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public void addPredicate(Predicate predicate) {
        /*if (selfPredicates == null) {
            selfPredicates = new ArrayList<>();
        }*/
        selfPredicates.add(predicate);
    }

    @Override
    public void setProperty(String key, Object value) {
        this.properties.set(key, value);
    }

    @Override
    public boolean hasPredicateSet() {
        return selfPredicates.size() > 0;
    }

    @Override
    public Collection<Predicate> getPredicates() {
        return this.selfPredicates;
    }

    public <E extends Element> boolean validatePredicate(E e) {
        boolean result = true;
        for (Predicate p : this.selfPredicates) { // only comparisons we have here and with values
            if (result) {
                if (p.getClass() == Comparison.class) {
                    Comparison comparison = (Comparison) p;
                    ComparableExpression[] list = comparison.getComparableExpressions();
                    if (list[0].getClass() == PropertySelector.class && list[1].getClass() == Literal.class) {
                        PropertySelector propertySelector = (PropertySelector) list[0];
                        PropertyValue LeftPropertyValue = e.getProperties().get(propertySelector.getPropertyName());
                        if (LeftPropertyValue != null) { // check here if right
                            Literal literal = (Literal) list[1];
                            PropertyValue rightPropertyValue = PropertyValue.create(literal.getValue());
                            result = evaluate(comparison, LeftPropertyValue, rightPropertyValue);
                        } else {
                            return false; // found null instead of value to compare with
                        }
                    } /*else if (list[0].getClass() == PropertySelector.class && list[1].getClass() == PropertySelector.class) {TODO: this should be here or left in final stage?
                        PropertySelector propertySelector = (PropertySelector) list[0];
                        PropertyValue LeftPropertyValue = e.getProperties().get(propertySelector.getPropertyName());
                        if (LeftPropertyValue != null) { // check here if right
                            PropertySelector rightPropertySelector = (PropertySelector) list[1];
                            PropertyValue rightPropertyValue = e.getProperties().get(rightPropertySelector.getPropertyName());
                            result = evaluate(comparison, LeftPropertyValue, rightPropertyValue);
                        }
                    }*/
                }
            } else {// last round was false
                return false;
            }
        }
        return true;
    }

    private boolean evaluate(Comparison comparison, PropertyValue leftPropertyValue, PropertyValue rightPropertyValue) {
        switch (comparison.getComparator()) {
            case EQ:
                return leftPropertyValue.compareTo(rightPropertyValue) == 0;
            case NEQ:
                return leftPropertyValue.compareTo(rightPropertyValue) != 0;
            case GT:
                return leftPropertyValue.compareTo(rightPropertyValue) > 0;
            case LT:
                return leftPropertyValue.compareTo(rightPropertyValue) < 0;
            case GTE:
                return leftPropertyValue.compareTo(rightPropertyValue) >= 0;
            case LTE:
                return leftPropertyValue.compareTo(rightPropertyValue) <= 0;
            default:
                throw new IllegalStateException("Unexpected value: " + comparison.getComparator());
        }
    }
}
