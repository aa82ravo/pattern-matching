package edu.leipzig.streaming.graphs.patternmatching;

import edu.leipzig.streaming.graphs.patternmatching.impl.simulation.dual.functions.DualSimulation;
import edu.leipzig.streaming.graphs.patternmatching.impl.simulation.dual.functions.FilterCandidates;
import edu.leipzig.streaming.graphs.patternmatching.model.*;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.windowing.assigners.WindowAssigner;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class PatternMatcher implements Serializable {
    private Query GDLQuery;
    private Collection<StreamObject<QueryVertex, QueryEdge>> queryTriples;
    private DataStream<StreamObject<StreamVertex, StreamEdge>> inputStream;
    private WindowAssigner<Object, TimeWindow> window;

    public PatternMatcher(DataStream<StreamObject<StreamVertex, StreamEdge>> inputStream, WindowAssigner<Object,
            TimeWindow> window, String query, boolean withEdgeOrder, ArrayList<String> variableOrder) {
        this.inputStream = inputStream;
        this.window = window;
        GDLQuery = new Query(query, withEdgeOrder, variableOrder);
    }

    public PatternMatchResult execute() {
        if (GDLQuery.isEmpty()) {
            return new PatternMatchResult("Query has no elements!!", true);
        } else if (GDLQuery.isVertexOnly()) {
            // execute for vertex only
            DataStream<StreamObject<StreamVertex, StreamEdge>> candidatesStream = filterForVertices(GDLQuery.getVertices());
            return new PatternMatchResult("Vertex only query", false, candidatesStream.windowAll(window).process(new DualSimulation(GDLQuery)));
        } else {
            // execute for both
            DataStream<StreamObject<StreamVertex, StreamEdge>> candidatesStream = filterExactEdgesWithVertices(this.getQueryTriples());
            return new PatternMatchResult("Working..", false, candidatesStream.windowAll(window).process(new DualSimulation(GDLQuery)));
        }
    }

    private DataStream<StreamObject<StreamVertex, StreamEdge>> filterExactEdgesWithVertices(Collection<StreamObject<QueryVertex, QueryEdge>> queryTriples) {
        return inputStream.keyBy(StreamObject::getSourceId).filter(new FilterCandidates(this.queryTriples, false, null));
    }


    private DataStream<StreamObject<StreamVertex, StreamEdge>> filterForVertices(ArrayList<QueryVertex> queryVertices) {
        return inputStream.keyBy(StreamObject::getSourceId).filter(new FilterCandidates(null, true, queryVertices));
    }

    private Collection<StreamObject<QueryVertex, QueryEdge>> getQueryTriples() {
        if (this.queryTriples == null) { // not yet initialised
            this.queryTriples = GDLQuery.getTriples(); // this check will delay the execution until its really needed, and will be executed once
        }
        return this.queryTriples;
    }
}
