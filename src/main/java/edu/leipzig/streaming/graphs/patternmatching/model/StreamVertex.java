package edu.leipzig.streaming.graphs.patternmatching.model;

import org.gradoop.common.model.impl.properties.Properties;

public class StreamVertex extends Element {

    /**
     * Default constructor is necessary to apply to POJO rules.
     */
    public StreamVertex() {
    }

    /**
     * constructor
     */
    public StreamVertex(String id, String label) {
        super(id, label, Properties.create());
        // this.setHash(DigestUtils.sha1Hex(this.toString()));
    }

    /**
     * constructor with all fields
     */
    public StreamVertex(String id, String label, Properties properties) {
        super(id, label, properties);
        // this.setHash(DigestUtils.sha1Hex(this.toString()));
    }

    public StreamVertex(String label, Properties properties) {
        super(label, properties);
        // this.setHash(DigestUtils.sha1Hex(this.toString()));
    }

    public StreamVertex(String id, String label, Properties properties, String variable) {
        super(id, label, properties, variable);
    }

    public String toString() {
        return String.format("(%s)", super.toString());
    }
}
