package edu.leipzig.streaming.graphs.patternmatching.impl.simulation.dual.functions;

import edu.leipzig.streaming.graphs.patternmatching.model.*;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.properties.PropertyValue;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.s1ck.gdl.model.comparables.ComparableExpression;
import org.s1ck.gdl.model.comparables.ElementSelector;
import org.s1ck.gdl.model.comparables.Literal;
import org.s1ck.gdl.model.comparables.PropertySelector;
import org.s1ck.gdl.model.predicates.Predicate;
import org.s1ck.gdl.model.predicates.booleans.And;
import org.s1ck.gdl.model.predicates.booleans.Not;
import org.s1ck.gdl.model.predicates.booleans.Or;
import org.s1ck.gdl.model.predicates.booleans.Xor;
import org.s1ck.gdl.model.predicates.expressions.Comparison;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class DualSimulation extends PatternMatchImplementation {
    private final Query query;

    public DualSimulation(Query query) {
        this.query = query;
    }

    @Override
    public void open(Configuration parameters) throws Exception {
        super.open(parameters);
    }

    /**
     * Evaluates the window and outputs none or several elements.
     *
     * @param context  The context in which the window is being evaluated.
     * @param elements The elements in the window being evaluated.
     * @param out      A collector for emitting elements.
     */
    @Override
    public void process(Context context, Iterable<StreamObject<StreamVertex, StreamEdge>> elements, Collector<BasicGraph> out) {
        BasicGraph<StreamEdge, StreamVertex> result;
        if (query.isVertexOnly()) {
            result = executeForVertex(elements, context.window());
        } else {
            result = executeForEdgesWithVertices(context, elements);
        }
        out.collect(result);
    }

    private BasicGraph<StreamEdge, StreamVertex> executeForEdgesWithVertices(Context context, Iterable<StreamObject<StreamVertex, StreamEdge>> elements) {
        // get unique elements from stream
        Map<String, StreamVertex> vertexMap = new HashMap<>();
        Map<String, StreamEdge> edgeMap = new HashMap<>();
        for (StreamObject<StreamVertex, StreamEdge> candidate : elements) {
            vertexMap.put(candidate.getSource().getHash(), candidate.getSource());
            vertexMap.put(candidate.getTarget().getHash(), candidate.getTarget());
            StreamEdge e = candidate.getEdge();
            edgeMap.put(e.getHash(), e);
        }
        Collection<StreamVertex> candidateVertices = new ArrayList<>();
        // Copy to be able to delete during iteration; for optimisation, we didn't do this for vertices, as no need to double the memory requirement
        Map<String, StreamEdge> candidateEdges = new HashMap<>(edgeMap);

        for (StreamVertex candidateVertex : vertexMap.values()) {
            boolean stillMatch = checkParentsAndChildren(candidateVertex, query.getTriples(), elements);// TODO: check with vertexMap.values()
            /*Map<String, StreamVertex> m = new HashMap<String, StreamVertex>(vertexMap);// to remove self
            m.remove(candidateVertex.getHash());*/
            if (stillMatch && checkPredicateTree(candidateVertex, query.getPredicates(), vertexMap.values())) {
                candidateVertices.add(candidateVertex);
                // check performance
                if (!candidateVertex.isChecked()) {
                    this.histogram.update(new Date().getTime() - candidateVertex.get_processingTimestamp());
                    candidateVertex.setChecked();
                }
            } else {
                for (StreamEdge edge : edgeMap.values()) {
                    if (edge.getTargetId().equals(candidateVertex.getId()) || edge.getSourceId().equals(candidateVertex.getId())) {
                        candidateEdges.remove(edge.getHash());
                        // check performance
                        if (!edge.isChecked()) {
                            this.histogram.update(new Date().getTime() - edge.get_processingTimestamp());
                            edge.setChecked();
                        }
                    }
                }
            }
        }
        if (!query.isVertexOnly() && candidateEdges.values().size() == 0) {// the fact that DS match anyway
            return new BasicGraph<StreamEdge, StreamVertex>(new HashMap<String, StreamEdge>().values(), new ArrayList<>(), context.window());
        }
        return new BasicGraph<>(candidateEdges.values(), candidateVertices, context.window());
    }

    private boolean checkPredicateTree(StreamVertex currentCandidateVertex, Predicate predicates, Collection<StreamVertex> candidatesInWindow) {
        if (predicates.getArguments().length > 1) {
            boolean applyLeft = predicates.getArguments()[0].getVariables().stream().anyMatch(currentCandidateVertex.getVariables()::contains);
            boolean applyRight = predicates.getArguments()[1].getVariables().stream().anyMatch(currentCandidateVertex.getVariables()::contains);
            if (And.class.equals(predicates.getClass())) {
                if (applyLeft && applyRight) {
                    return checkPredicateTree(currentCandidateVertex, predicates.getArguments()[0], candidatesInWindow) &&
                            checkPredicateTree(currentCandidateVertex, predicates.getArguments()[1], candidatesInWindow);
                } else if (applyLeft) {
                    return checkPredicateTree(currentCandidateVertex, predicates.getArguments()[0], candidatesInWindow);
                } else {
                    return checkPredicateTree(currentCandidateVertex, predicates.getArguments()[1], candidatesInWindow);
                }
            } else if (Not.class.equals(predicates.getClass())) {
                return !checkPredicateTree(currentCandidateVertex, predicates.getArguments()[0], candidatesInWindow);
            } else if (Or.class.equals(predicates.getClass())) {
                if (applyLeft && applyRight) {
                    return checkPredicateTree(currentCandidateVertex, predicates.getArguments()[0], candidatesInWindow) ||
                            checkPredicateTree(currentCandidateVertex, predicates.getArguments()[1], candidatesInWindow);
                } else if (applyLeft) {
                    return checkPredicateTree(currentCandidateVertex, predicates.getArguments()[0], candidatesInWindow);
                } else {
                    checkPredicateTree(currentCandidateVertex, predicates.getArguments()[1], candidatesInWindow);
                }
            } else if (Xor.class.equals(predicates.getClass())) {
                if (applyLeft && applyRight) {
                    return checkPredicateTree(currentCandidateVertex, predicates.getArguments()[0], candidatesInWindow) ^
                            checkPredicateTree(currentCandidateVertex, predicates.getArguments()[1], candidatesInWindow);
                } else if (applyLeft) {
                    return checkPredicateTree(currentCandidateVertex, predicates.getArguments()[0], candidatesInWindow);
                } else {
                    return checkPredicateTree(currentCandidateVertex, predicates.getArguments()[1], candidatesInWindow);
                }
            }
        } else if (Comparison.class.equals(predicates.getClass())) { // comparison has no further predicates
            Comparison comparison = (Comparison) predicates;
            ComparableExpression left = comparison.getComparableExpressions()[0];
            ComparableExpression right = comparison.getComparableExpressions()[1];
            if (comparison.getVariables().size() == 1) {
                return compareSingleVariable(currentCandidateVertex, comparison, left, right);
            } else {
                Collection<StreamVertex> streamVerticesToCompareWith = getVerticesToCompareWith(currentCandidateVertex, candidatesInWindow, comparison);
                if (left.getClass().equals(PropertySelector.class) && right.getClass().equals(PropertySelector.class)) {// TODO:exclude the case where it may be evaluated in edge self predicates
                    return compareWithPropertySelector(currentCandidateVertex, comparison, left, (PropertySelector) right, streamVerticesToCompareWith);
                } else if (left.getClass().equals(ElementSelector.class) && right.getClass().equals(ElementSelector.class)) {
                    return compareWithElementSelector(currentCandidateVertex, comparison, streamVerticesToCompareWith);
                }
            }
        }
        return true;// no comparison? error? fall back to dual simulation?
    }

    private boolean compareSingleVariable(StreamVertex currentCandidateVertex, Comparison comparison, ComparableExpression left, ComparableExpression right) {
        PropertyValue leftValue = null;
        PropertyValue rightValue = null;
        if (left.getClass().equals(PropertySelector.class)) {// the other is Literal
            if (((PropertySelector) left).getPropertyName().equals("__label__")) {
                return true;
            }
            leftValue = currentCandidateVertex.getProperties().get(((PropertySelector) left).getPropertyName());
            if (right.getClass().equals(Literal.class)) {
                rightValue = PropertyValue.create(((Literal) right).getValue());
            }
        } else if (left.getClass().equals(Literal.class)) {
            leftValue = PropertyValue.create(((Literal) left).getValue());
            if (right.getClass().equals(PropertySelector.class)) {
                if (((PropertySelector) right).getPropertyName().equals("__label__")) {
                    return true;
                }
                rightValue = currentCandidateVertex.getProperties().get(((PropertySelector) right).getPropertyName());
            }
        }
        return executeComparison(comparison, leftValue, rightValue);
        // return true; // as we are in window and all single predicate would be passes in the filter //TODO: check this
    }

    @NotNull
    private Collection<StreamVertex> getVerticesToCompareWith(StreamVertex currentCandidateVertex, Collection<StreamVertex> candidatesInWindow, Comparison comparison) {
        String otherVariable = null;
        for (String var : comparison.getVariables()) {
            for (String candidateVariable : currentCandidateVertex.getVariables()) {
                if (!var.equals(candidateVariable)) {// TODO: check
                    otherVariable = var;
                    break;
                }
            }

        }
        String finalOtherVariable = otherVariable;
        return candidatesInWindow.stream().filter(element ->
                element.hasVariable(finalOtherVariable)).collect(Collectors.toList());
    }

    @Nullable
    private boolean compareWithPropertySelector(StreamVertex currentCandidateVertex, Comparison comparison, ComparableExpression left, PropertySelector right, Collection<StreamVertex> streamVerticesToCompareWith) {
        PropertyValue leftValue;
        PropertyValue rightValue;
        if (currentCandidateVertex.hasVariable(left.getVariable())) {
            leftValue = currentCandidateVertex.getProperties().get(((PropertySelector) left).getPropertyName());
            boolean result = false;
            for (StreamVertex opponent : streamVerticesToCompareWith) {
                rightValue = opponent.getProperties().get(right.getPropertyName());
                if (leftValue != null && rightValue != null) {
                    if (executeComparison(comparison, leftValue, rightValue)) {
                        result = true;
                        // currentCandidateVertex.getMatchingIds().add(opponent.getId());
                    }
                }
            }
            return result;
        } else {
            leftValue = currentCandidateVertex.getProperties().get(((PropertySelector) left).getPropertyName());
            boolean result = false;
            for (StreamVertex opponent : streamVerticesToCompareWith) {
                rightValue = opponent.getProperties().get(right.getPropertyName());
                if (leftValue != null && rightValue != null) {
                    if (executeComparison(comparison, rightValue, leftValue)) {
                        result = true;
                        // currentCandidateVertex.getMatchingIds().add(opponent.getId());
                    }
                }
            }
            return result;
        }
    }

    private boolean compareWithElementSelector(StreamVertex currentCandidateVertex, Comparison comparison, Collection<StreamVertex> streamVerticesToCompareWith) {
        PropertyValue leftValue;
        PropertyValue rightValue;
        leftValue = PropertyValue.create(currentCandidateVertex.getId());
        boolean result = false;
        for (StreamVertex opponent : streamVerticesToCompareWith) {
            rightValue = PropertyValue.create(opponent.getId());
            if (executeComparison(comparison, rightValue, leftValue)) {
                result = true;
                // currentCandidateVertex.getMatchingIds().add(opponent.getId());
            }
        }
        return result;
    }

    private boolean executeComparison(Comparison comparison, PropertyValue leftValue, PropertyValue rightValue) {
        if (leftValue == null || rightValue == null) {
            return false;
        }
        switch (comparison.getComparator()) {
            case EQ:
                return leftValue.compareTo(rightValue) == 0;
            case NEQ:
                return leftValue.compareTo(rightValue) != 0;
            case GT:
                return leftValue.compareTo(rightValue) > 0;
            case LT:
                return leftValue.compareTo(rightValue) < 0;
            case GTE:
                return leftValue.compareTo(rightValue) >= 0;
            case LTE:
                return leftValue.compareTo(rightValue) <= 0;
            default:
                throw new IllegalStateException("Unexpected value: " + comparison.getComparator());
        }
    }

    private BasicGraph<StreamEdge, StreamVertex> executeForVertex(Iterable<StreamObject<StreamVertex, StreamEdge>> elements, TimeWindow window) {
        // get unique elements from stream
        Map<String, StreamVertex> candidateVertices = new HashMap<>();
        for (StreamObject<StreamVertex, StreamEdge> candidate : elements) {
            for (StreamVertex v : query.getVertices()) {
                if (v.innerMatch(candidate.getSource())) {
                    candidateVertices.put(candidate.getSource().getHash(), candidate.getSource());
                }
                if (v.innerMatch(candidate.getTarget())) {
                    candidateVertices.put(candidate.getTarget().getHash(), candidate.getTarget());
                }
            }
        }
        return new BasicGraph<>(new ArrayList<>(), candidateVertices.values(), window);
    }

    private boolean checkParentsAndChildren(StreamVertex currentCandidateVertex, Collection<StreamObject<QueryVertex, QueryEdge>> queryTriples, Iterable<StreamObject<StreamVertex, StreamEdge>> candidatesInWindow) {
        Collection<StreamObject<QueryVertex, QueryEdge>> queryRelatives = queryTriples.stream().filter(element ->
                (element.getSource().innerMatch(currentCandidateVertex) || element.getTarget().innerMatch(currentCandidateVertex))).collect(Collectors.toList());
        List<StreamObject<StreamVertex, StreamEdge>> candidateRelatives = StreamSupport.stream(candidatesInWindow.spliterator(), false).filter(element ->
                (element.getSource().innerMatch(currentCandidateVertex) || element.getTarget().innerMatch(currentCandidateVertex))).collect(Collectors.toList());
        for (StreamObject<QueryVertex, QueryEdge> relative : queryRelatives) {
            boolean exist = false;
            for (StreamObject<StreamVertex, StreamEdge> candidate : candidateRelatives) {
                exist = candidate.equals(relative);
                if (exist) {
                        break;
                }
            }
            if (!exist) {
                return false;
            }
        }
        return true;
    }
}
