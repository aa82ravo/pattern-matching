package edu.leipzig.streaming.graphs.patternmatching.model;

import org.apache.commons.codec.digest.DigestUtils;
import org.gradoop.common.model.impl.properties.Properties;
import org.gradoop.common.model.impl.properties.Property;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public abstract class Element implements Serializable {
    protected String id;
    protected String label;
    protected Properties properties;
    protected ArrayList<String> variables = new ArrayList<>();
    boolean checked = false;

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    Long timestamp;

    public Element() {
    }

    public Long get_processingTimestamp() {
        return _processingTimestamp;
    }

    public void set_processingTimestamp(Long _processingTimestamp) {
        this._processingTimestamp = _processingTimestamp;
    }

    Long _processingTimestamp; //used for performance metrics

    protected Element(String id, String label, Properties properties) {
        this.id = id;
        this.label = label;
        this.properties = properties;
    }

    protected Element(String label, Properties properties) {
        this.label = label;
        this.properties = properties;
    }


    protected Element(String id, String label, Properties properties, String variable) {
        this.id = id;
        this.label = label;
        this.properties = properties;
        this.variables.add(variable);
    }

    public Collection<String> getVariables() {
        return variables;
    }

    /*    public void setVariable(String variable) {
            this.variable = Collections.singleton(variable);
        }*/
    public void addVariable(String variable) {
        this.variables.add(variable);
    }

    public boolean hasVariable(String variable) {
        boolean result = false;
        for (String var : this.variables) {
            if (var.equals(variable)) {
                result = true;
                break;
            }
        }
        return result;
    }

    public <E extends Element> boolean innerMatch(E element) {
        return this.innerMatch(element, this, false);
    }

    <E extends Element> boolean innerMatch(E element1, E element2) {
        return this.innerMatch(element1, element2, false);
    }

    <E extends Element> boolean innerMatch(E element1, E element2, boolean exact) {

        boolean match = false;
        // verify label
        if (element2.getLabel().equals(element1.getLabel()) || element2.getLabel().equals("__VERTEX") || element1.getLabel().equals("__VERTEX")) {
            match = true;
        }
        // verify properties
        if (match) {
            if (exact && element2.getProperties() == null && element1.getProperties() == null) {
                return true;
            }
            if (element2.getProperties() != null && element1.getProperties() != null) {
                Iterator<Property> theSmallOne;
                Properties theBigOne;
                if (element2.getProperties().size() < element1.getProperties().size()) {
                    if (exact) { //not the same size
                        return false;
                    }
                    theSmallOne = element2.getProperties().iterator();
                    theBigOne = element1.getProperties();
                } else {
                    theSmallOne = element1.getProperties().iterator();
                    theBigOne = element2.getProperties();
                }
                while (match && theSmallOne.hasNext()) {
                    Property next = theSmallOne.next();
                    // if the property key is not valid, it is not a match
                    if (!theBigOne.containsKey(next.getKey())) {
                        match = false;
                        break;
                    }
                    match = theBigOne.get(next.getKey()).getObject()
                            .equals(next.getValue().getObject());
                }
            }
        }

        return match;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    public String toString() {
        return String.format("%s%s%s{%s}", this.id, this.label != null && !this.label.equals("") ? ":" : "", this.label, this.properties == null ? "" : this.properties);
    }

    @Override
    public boolean equals(Object o) {
        if (getClass() != o.getClass()) {
            return false;
        }

        if (this == o) {
            return true;
        }
        Element element = (Element) o;
        return innerMatch(element, this);
    }

    public String getHash() {
        return DigestUtils.sha1Hex(this.toString());
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked() {
        checked = true;
    }
}
