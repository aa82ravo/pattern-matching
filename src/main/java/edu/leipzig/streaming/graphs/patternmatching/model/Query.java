package edu.leipzig.streaming.graphs.patternmatching.model;

import org.gradoop.common.model.impl.properties.Properties;
import org.jetbrains.annotations.NotNull;
import org.s1ck.gdl.GDLHandler;
import org.s1ck.gdl.model.Edge;
import org.s1ck.gdl.model.GraphElement;
import org.s1ck.gdl.model.Vertex;
import org.s1ck.gdl.model.comparables.Literal;
import org.s1ck.gdl.model.comparables.PropertySelector;
import org.s1ck.gdl.model.predicates.Predicate;
import org.s1ck.gdl.model.predicates.booleans.And;
import org.s1ck.gdl.model.predicates.expressions.Comparison;
import org.s1ck.gdl.utils.Comparator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

public class Query extends BasicGraph<QueryEdge, QueryVertex> {

    private Predicate predicates;
    private boolean isAndOnly;

    public Query() {
        super();
    }

    public Query(String queryString, boolean withEdgeOrder, ArrayList<String> variableOrder) {
        // "g[(alice)-[e1:knows {since : 2014}]->(bob)]"
        GDLHandler handler = initQueryHandler(queryString);
        extractQueryVertices(handler);
        extractQueryEdges(handler, withEdgeOrder, variableOrder);
        if (withEdgeOrder) {
            addOrderToPredicates(handler);
        }
    }

    private void addOrderToPredicates(GDLHandler handler) {
        Comparison comparison;
        Predicate newPredicates = this.getPredicates();
        for (int i = 0; i < this.edges.size() - 1; i++) {
            QueryEdge e = this.edges.get(i);
            QueryEdge eNext = this.edges.get(i + 1);
            if (e.getOrder() < eNext.getOrder()) {
                comparison = new Comparison(new PropertySelector(e.getVariable(), "timestamp"), Comparator.LT,
                        new PropertySelector(eNext.getVariable(), "timestamp"));
            } else {
                comparison = new Comparison(new PropertySelector(e.getVariable(), "timestamp"), Comparator.GT,
                        new PropertySelector(eNext.getVariable(), "timestamp"));
            }
            newPredicates = new And(newPredicates, comparison);
        }
        if (newPredicates != null) {
            this.predicates = newPredicates;
        }
    }

    public Predicate getPredicates() {
        return predicates;
    }

    @NotNull
    private GDLHandler initQueryHandler(String queryString) {
        GDLHandler handler = new GDLHandler.Builder().buildFromString(queryString);
        handler.getPredicates().ifPresent(this::SetPredicates);
        if (this.predicates != null) {
            System.out.print(predicates.getVariables());
            for (Predicate p : this.predicates.getArguments()) {
                System.out.println(p);
            }
        }
        return handler;
    }

    private void extractQueryEdges(GDLHandler handler, boolean withEdgeOrder, ArrayList<String> variableOrder) {
        int idx = 0;
        for (Edge e : handler.getEdges()) {
            idx++;
            // String id, Long timestamp, String label, Properties properties, String sourceId, String targetId
            QueryEdge tempEdge = new QueryEdge(String.valueOf(e.getId()), null, e.getLabel(), Properties.createFromMap(e.getProperties()),
                    String.valueOf(e.getSourceVertexId()), String.valueOf(e.getTargetVertexId()), e.getVariable());
            // tempEdge.setOrder(variableOrder.indexOf(e.getVariable()));
            if (withEdgeOrder) {
                tempEdge.setOrder(variableOrder.indexOf(e.getVariable()));
            }
            if (this.predicates != null) {
                for (Predicate a : this.predicates.getArguments()) {
                    searchTreeForProperty(e, tempEdge, a);
                }
            }

            this.edges.add(tempEdge);
        }
    }

    private void extractQueryVertices(GDLHandler handler) {
        for (Vertex v : handler.getVertices()) {
            QueryVertex tempVertex = new QueryVertex(String.valueOf(v.getId()), v.getLabel(), Properties.createFromMap(v.getProperties()), v.getVariable());
            // String id, String label, Properties properties
            if (this.predicates != null) {
                for (Predicate a : this.predicates.getArguments()) {
                    searchTreeForProperty(v, tempVertex, a);
                }
            }
            this.vertices.add(tempVertex);
        }
    }

    private void searchTreeForProperty(GraphElement v, HasPredicate tempVertex, Predicate a) {
        if (a.getClass() == And.class) {// comparison?
            for (Predicate aa : a.getArguments()) {
                searchTreeForProperty(v, tempVertex, aa);
            }
        }
        if (a.getVariables().contains(v.getVariable())) {
            if (a.getClass() == Comparison.class) {
                if (a.getVariables().size() == 1) { // has info only about this object
                    Comparison ca = (Comparison) a;
                    if (ca.getComparator().equals(Comparator.EQ)) {
                        if (ca.getComparableExpressions()[0].getClass() == PropertySelector.class &&
                                ca.getComparableExpressions()[1].getClass() == Literal.class) {
                            PropertySelector ps = (PropertySelector) ca.getComparableExpressions()[0];
                            Literal ls = (Literal) ca.getComparableExpressions()[1];
                            String propertyName = ps.getPropertyName();
                            Object propertyValue = ls.getValue();
                            if (!propertyName.equals("__label__")) {
                                tempVertex.setProperty(propertyName, propertyValue);
                            }
                        }
                    } else {
                        tempVertex.addPredicate(a);
                    }
                } //was here
            }
        } else if (a.getVariables().size() == 2 && tempVertex.getClass() == QueryEdge.class) { // edge: then look for direct neighbor expressions
            Optional<QueryVertex> relatedSourceVertex = this.vertices.stream().filter(element ->
                    element.getId().equals(((QueryEdge) tempVertex).getSourceId())).findAny();
            Optional<QueryVertex> relatedTargetVertex = this.vertices.stream().filter(element ->
                    element.getId().equals(((QueryEdge) tempVertex).getTargetId())).findAny();
            if (relatedSourceVertex.isPresent() && relatedTargetVertex.isPresent()) {
                Collection<String> variables = new ArrayList<>();
                variables.add(relatedTargetVertex.get().getVariable());
                variables.add(relatedSourceVertex.get().getVariable());
                if (a.getVariables().containsAll(variables)) {
                    tempVertex.addPredicate(a);
                }
            }
        }
    }

    private void SetPredicates(Predicate predicate) {
        this.predicates = predicate;
        this.isAndOnly = this.queryPredicatesOnlyAnd(this.predicates);
    }

    private boolean queryPredicatesOnlyAnd(@NotNull Predicate predicate) {
        boolean result;
        if (predicate.getClass() != And.class && predicate.getClass() != Comparison.class) {
            return false;
        } else {
            result = true;
            for (Predicate p : predicate.getArguments()) {
                result = result && queryPredicatesOnlyAnd(p);
            }
        }
        return result;
    }

    /*public Query getSample() {
        // "g[(v2: Developer {name : "Arnulfo Brekke"})-[e1:MEMBER_OF ]->(v1:Company {name : "McKenzie LLC"}),(v3: Project {name : "Sonsing"})-[e2:BELONGS_TO ]->(v1), (v2)-[e3:COMMIT ]->(v3)]"
        //
        StreamVertex v1 = new StreamVertex();
        v1.setId("1L");
        v1.setLabel("Company");
        Properties p1 = new Properties();
        p1.set("name", "McKenzie LLC");
        v1.setProperties(p1);
        StreamVertex v2 = new StreamVertex();
        v2.setLabel("Developer");
        v2.setId("2L");
        Properties p2 = new Properties();
        p2.set("name", "Arnulfo Brekke");
        v2.setProperties(p2);
        StreamEdge e1 = new StreamEdge();
        e1.setLabel("MEMBER_OF");
        e1.setId("3L");
        e1.setSourceId(v2.getId());
        e1.setTargetId(v1.getId());

        StreamVertex v3 = new StreamVertex();
        v3.setLabel("Project");
        v3.setId("4L");
        Properties p3 = new Properties();
        p3.set("name", "Sonsing");
        v3.setProperties(p3);

        StreamEdge e2 = new StreamEdge();
        e2.setLabel("BELONGS_TO");
        e2.setId("5L");
        e2.setSourceId(v3.getId());
        e2.setTargetId(v1.getId());

        StreamEdge e3 = new StreamEdge();
        e3.setLabel("COMMIT");
        e3.setId("6L");
        e3.setSourceId(v2.getId());
        e3.setTargetId(v3.getId());

        Query query = new Query();
        query.addEdge(e1);
        query.addEdge(e2);
        query.addEdge(e3);
        query.addVertex(v1);
        query.addVertex(v2);
        query.addVertex(v3);
        return query;
    }*/

    public boolean isVertexOnly() {
        if (!this.isEmpty()) {
            return this.edges.size() == 0;
        }
        return false;
    }

    public boolean isEmpty() {
        return this.edges.size() == 0 && this.vertices.size() == 0;
    }

}