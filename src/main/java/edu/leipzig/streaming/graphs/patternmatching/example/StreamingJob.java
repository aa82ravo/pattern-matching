/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.leipzig.streaming.graphs.patternmatching.example;

import edu.leipzig.streaming.graphs.patternmatching.PatternMatcher;
import edu.leipzig.streaming.graphs.patternmatching.example.functions.JSONMapperWithCounter;
import edu.leipzig.streaming.graphs.patternmatching.model.*;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;
import org.apache.flink.streaming.api.functions.timestamps.AscendingTimestampExtractor;
import org.apache.flink.streaming.api.windowing.assigners.SlidingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Skeleton for a Flink Streaming Job.
 *
 * <p>For a tutorial how to write a Flink streaming application, check the
 * tutorials and examples on the <a href="http://flink.apache.org/docs/stable/">Flink Website</a>.
 *
 * <p>To package your application into a JAR file for execution, run
 * 'mvn clean package' on the command line.
 *
 * <p>If you change the name of the main class (with the public static void main(String[] args))
 * method, change the respective entry in the POM.xml file (simply search for 'mainClass').
 */
public class StreamingJob {

    private static String hostname;
    private static String path;

    // args: run-config, window size in seconds, window slide in seconds
    public static void main(String[] args) throws Exception {
        String ex1 = "MATCH (:Person)-[:likes]->(p:Person) WHERE p.name = \"Christopher\"";
        // String ex2 = "MATCH (a:Person)-[:knows]->(b:Person) WHERE a.age > b.age AND a.name = \"Christopher\" AND a.age < 35";
        String ex2 = "MATCH (a) -[b:MEMBER_OF]->(c:Company) WHERE a.name = \"Garrick Gulgowski\" AND a.salary < 5000.0d AND c.name = \"Herman-Herman\" and c.city != \"Leipzig\" and b.month = \"Nov\"";
        String ex4 = "MATCH (a) -[b:MEMBER_OF]->(c:Company), (a)-[d:COMMIT]->(f:Project), (g)-[:COMMIT]->(f), (a)-[:KNOWS]->(g) WHERE a.salary < g.salary AND a.salary < 5000.0d AND c.city = \"Labadiefurt\" and b.month = \"Jul\"";
        String ex4OR = "MATCH (a) -[b:MEMBER_OF]->(c:Company), (a)-[d:COMMIT]->(f:Project), (g)-[:COMMIT]->(f), (a)-[:KNOWS]->(g) WHERE (a.salary > g.salary AND a.salary < 5000.0d AND b.month = \"Nov\") OR c.city = \"Leipzig\"";
        String ex5 = "MATCH (a) -[b:MEMBER_OF]->(c:Company), (a)-[d:COMMIT]->(f:Project), (g)-[:COMMIT]->(f) WHERE a.salary > g.salary AND a.name = \"Jalyn Boyer\" AND a.salary < 5000.0d AND c.name = \"Stoltenberg and Sons\" and c.city != \"Leipzig\" and b.month = \"Nov\"";
        String ex3 = "g[({name:\"Alysson Wehner\", salary:2094.0d})]";
        String loop = "MATCH (a:Developer) -[b:KNOWS ]->(c) WHERE a.name=c.name";
        String simple = "MATCH (a:Developer) -[b:KNOWS ]->(c)";
        String minimumMatch = "MATCH (a:someThingNotThere) -[b:test ]->(c:someThingNotThere)";
        // String twet="MATCH (a:Developer)-[b:COMMIT]->(c:Project), (c1:Project)<-[d:COMMIT]-(e:Developer) WHERE a = e AND c <> c1";
        String twet = "MATCH (a:Developer)-[b:MEMBER_OF]->(c:Company)<-[d:MEMBER_OF]-(e:Developer) WHERE a <> e";
        // String ex2 = "MATCH (a:Person)-[:knows]->(b:Person) WHERE a.name = \"Christopher\"";

        String query;
        long windowSize;
        long windowSlideSize;
        switch (Integer.parseInt(args[0])) {
            case 1:
                query = ex4OR;
                break;
            case 2:
                query = ex4;
                break;
            case 3:
                query = minimumMatch;
                break;
            default:
                throw new IllegalStateException("Unexpected value for first input: " + Integer.parseInt(args[0]));
        }
        try {
            hostname = args[3];
        } catch (Exception e) {
            System.out.println("no hostname will use localhost");
            hostname = "localhost";
            e.printStackTrace();
        }
        try {
            windowSize = Long.parseLong(args[1]);
            windowSlideSize = Long.parseLong(args[2]);
        } catch (Exception e) {
            System.out.println("Unexpected value for first window settings:");
            windowSize = 20L;
            windowSlideSize = 10L;
            e.printStackTrace();
        }
        try {
            path = args[4];
        } catch (Exception e) {
            System.out.println("Unexpected value for file path");
            e.printStackTrace();
        }
        System.out.println(String.format("Running with\nQuery:%s\nwindowSize: %d sec, windowSlideSize: %d", query, windowSize, windowSlideSize));
        runExample(hostname, 6661, query, windowSize, windowSlideSize);
    }

    static void runExample(String hostname, int port, String query, long windowSize, long windowSlideSize) throws Exception {
        // set up the streaming execution environment================
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        //env.getConfig().setLatencyTrackingInterval(1000);
		 /*
        connects to a socket source stream
        * */
        DataStream<StreamObject<StreamVertex, StreamEdge>> socketStream =
                env.socketTextStream(hostname, port)
                        .map(new JSONMapperWithCounter()).assignTimestampsAndWatermarks(
                        new AscendingTimestampExtractor<StreamObject<StreamVertex, StreamEdge>>() {
                            @Override
                            public long extractAscendingTimestamp(StreamObject<StreamVertex, StreamEdge> element) {
                                return element.getTimestamp();
                            }
                        });
        /*DataStream<StreamObject<StreamVertex, StreamEdge>> socketStream =
                env.readTextFile(path)
                        .map(new JSONMapperWithCounter()).assignTimestampsAndWatermarks(
                        new AscendingTimestampExtractor<StreamObject<StreamVertex, StreamEdge>>() {
                            @Override
                            public long extractAscendingTimestamp(StreamObject<StreamVertex, StreamEdge> element) {
                                return element.getTimestamp();
                            }
                        });*/

        String[] order = {"a", "b"};
        PatternMatcher matcher = new PatternMatcher(socketStream, SlidingEventTimeWindows.of(Time.milliseconds(windowSize)  /* .seconds(windowSize)*/,
                Time.milliseconds(windowSlideSize)/*  .seconds(windowSlideSize)*/), query, false, new ArrayList<>(Arrays.asList(order)));
        PatternMatchResult result = matcher.execute();
        result.getResultStream().addSink(new SinkFunction<BasicGraph>() {
            @Override
            public void invoke(BasicGraph value, Context context) throws Exception {
                String s = value.getJSONString();
                s = s + "d";
            }
        });

        //String x =
        env.execute("Pattern Matcher");
    }

}
