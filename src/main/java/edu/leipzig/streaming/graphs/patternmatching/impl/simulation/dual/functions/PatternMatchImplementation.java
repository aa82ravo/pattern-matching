package edu.leipzig.streaming.graphs.patternmatching.impl.simulation.dual.functions;

import com.codahale.metrics.UniformReservoir;
import edu.leipzig.streaming.graphs.patternmatching.model.BasicGraph;
import edu.leipzig.streaming.graphs.patternmatching.model.StreamEdge;
import edu.leipzig.streaming.graphs.patternmatching.model.StreamObject;
import edu.leipzig.streaming.graphs.patternmatching.model.StreamVertex;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.dropwizard.metrics.DropwizardHistogramWrapper;
import org.apache.flink.metrics.Histogram;
import org.apache.flink.streaming.api.functions.windowing.ProcessAllWindowFunction;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

public class PatternMatchImplementation extends ProcessAllWindowFunction<StreamObject<StreamVertex, StreamEdge>, BasicGraph, TimeWindow> {
    transient Histogram histogram;

    @Override
    public void open(Configuration parameters) throws Exception {
        super.open(parameters);
        com.codahale.metrics.Histogram dropwizardHistogram =
                new com.codahale.metrics.Histogram(new UniformReservoir());
        this.histogram = getRuntimeContext()
                .getMetricGroup()
                .histogram("AbdHistogram", new DropwizardHistogramWrapper(dropwizardHistogram));
    }

    /**
     * Evaluates the window and outputs none or several elements.
     *
     * @param context  The context in which the window is being evaluated.
     * @param elements The elements in the window being evaluated.
     * @param out      A collector for emitting elements.
     * @throws Exception The function may throw exceptions to fail the program and trigger recovery.
     */
    @Override
    public void process(Context context, Iterable<StreamObject<StreamVertex, StreamEdge>> elements, Collector<BasicGraph> out) throws Exception {

    }
}
