package edu.leipzig.streaming.graphs.patternmatching.model;

import org.apache.flink.streaming.api.datastream.DataStream;

public class PatternMatchResult {
    private String status;

    public boolean getHasError() {
        return hasError;
    }

    private boolean hasError;
    private DataStream<BasicGraph> resultStream;

    public PatternMatchResult(String status, boolean hasError, DataStream<BasicGraph> resultStream) {
        this.status = status;
        this.hasError = hasError;
        this.resultStream = resultStream;
    }

    public PatternMatchResult(String status, boolean hasError) {
        this.status = status;
        this.hasError = hasError;
    }

    public String getStatus() {
        return status;
    }

    public DataStream<BasicGraph> getResultStream() {
        return resultStream;
    }
}
