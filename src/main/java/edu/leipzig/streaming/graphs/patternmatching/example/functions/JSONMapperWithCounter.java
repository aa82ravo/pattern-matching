package edu.leipzig.streaming.graphs.patternmatching.example.functions;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import edu.leipzig.streaming.graphs.patternmatching.model.StreamEdge;
import edu.leipzig.streaming.graphs.patternmatching.model.StreamObject;
import edu.leipzig.streaming.graphs.patternmatching.model.StreamVertex;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.dropwizard.metrics.DropwizardMeterWrapper;
import org.apache.flink.metrics.Counter;
import org.apache.flink.metrics.Gauge;
import org.apache.flink.metrics.Meter;
import org.gradoop.common.model.impl.properties.Properties;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;

/**
 * Parses the JSON incoming from the generator, map it to the corresponding StreamEdge instance
 */


class myGauge implements Gauge<Long> {
    private long lastCount = 0;
    private long lastTimestamp = new Date().getTime();
    private long currentTimestamp;
    private long currentCount;

    /**
     * Calculates and returns the measured value.
     *
     * @return calculated value
     */

    void update(long count, long timestamp) {
        this.lastTimestamp = this.currentTimestamp;
        this.currentTimestamp = timestamp;
        this.lastCount = this.currentCount;
        this.currentCount = count;
    }

    @Override
    public Long getValue() {
        long count = currentCount - lastCount;
        long millis = currentTimestamp - lastTimestamp;
        long secs = millis / 1000L;
        return count / millis;
    }
}

public class JSONMapperWithCounter extends RichMapFunction<String, StreamObject<StreamVertex, StreamEdge>> {
    /**
     * Field name of id
     */
    private static final String ID = "id";
    /**
     * Field name of label
     */
    private static final String LABEL = "label";
    /**
     * Field name of properties
     */
    private static final String PROPERTIES = "properties";
    /**
     * Field name of source
     */
    private static final String SOURCE = "source";
    /**
     * Field name of source
     */
    private static final String TARGET = "target";
    /**
     * Field name of timestamp
     */
    private static final String TIMESTAMP = "timestamp";
    private transient Counter counter;
    private transient Meter meter;
    private transient Gauge gauge;
    static com.codahale.metrics.Meter dropwizardMeter = new com.codahale.metrics.Meter();
    static DropwizardMeterWrapper wrpper= new DropwizardMeterWrapper(dropwizardMeter);
    public JSONMapperWithCounter() {
    }

    @Override
    public void open(Configuration config) {

        //com.codahale.metrics.RatioGauge
        /*this.counter = getRuntimeContext()
                .getMetricGroup()
                .counter("newElements");*/
        this.meter = getRuntimeContext()
                .getMetricGroup()
                .meter("myMeter", wrpper);
        /*this.gauge = getRuntimeContext()
                .getMetricGroup().gauge("MyPerSecGauge", new myGauge());*/

    }

    public StreamObject<StreamVertex, StreamEdge> map(String s) {
        JsonElement root = new JsonParser().parse(s);
        Gson g = new Gson();
        StreamVertex source;
        StreamVertex target;
        source = new StreamVertex(root.getAsJsonObject().get(SOURCE).getAsJsonObject().get(ID).getAsString(),
                root.getAsJsonObject().get(SOURCE).getAsJsonObject().get(LABEL).getAsString(),
                Properties.createFromMap(g.fromJson(
                        root.getAsJsonObject().get(SOURCE).getAsJsonObject().get(PROPERTIES),
                        new TypeToken<HashMap<String, Object>>() {
                        }.getType()
                        )
                )
        );
        source.setTimestamp(root.getAsJsonObject().get(TIMESTAMP).getAsLong());
        source.set_processingTimestamp(new Timestamp(System.currentTimeMillis()).getTime());
        target = new StreamVertex(root.getAsJsonObject().get(TARGET).getAsJsonObject().get(ID).getAsString(),
                root.getAsJsonObject().get(TARGET).getAsJsonObject().get(LABEL).getAsString(),
                Properties.createFromMap(g.fromJson(
                        root.getAsJsonObject().get(TARGET).getAsJsonObject().get(PROPERTIES),
                        new TypeToken<HashMap<String, Object>>() {
                        }.getType()
                        )
                )
        );
        target.setTimestamp(root.getAsJsonObject().get(TIMESTAMP).getAsLong());
        target.set_processingTimestamp(new Timestamp(System.currentTimeMillis()).getTime());
        StreamEdge edge = new StreamEdge(root.getAsJsonObject().get(ID).getAsString(),
                root.getAsJsonObject().get(TIMESTAMP).getAsLong(),
                root.getAsJsonObject().get(LABEL).getAsString(),
                Properties.createFromMap(g.fromJson(
                        root.getAsJsonObject().get(PROPERTIES),
                        new TypeToken<HashMap<String, Object>>() {
                        }.getType()
                        )
                ), source.getId(), target.getId());
        edge.set_processingTimestamp(new Timestamp(System.currentTimeMillis()).getTime());
        //this.counter.inc();
        this.meter.markEvent();
        return new StreamObject<StreamVertex, StreamEdge>(edge, source, target);
    }
}
