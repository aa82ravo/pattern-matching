package edu.leipzig.streaming.graphs.patternmatching.example.functions;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import edu.leipzig.streaming.graphs.patternmatching.model.StreamEdge;
import edu.leipzig.streaming.graphs.patternmatching.model.StreamObject;
import edu.leipzig.streaming.graphs.patternmatching.model.StreamVertex;
import org.apache.flink.api.common.functions.MapFunction;
import org.gradoop.common.model.impl.properties.Properties;

import java.util.HashMap;

/**
 * Parses the JSON incoming from the generator, map it to the corresponding StreamEdge instance
 */
public class JSONToStreamGramMapper implements MapFunction<String, StreamObject<StreamVertex, StreamEdge>> {

    /**
     * Field name of id
     */
    private static final String ID = "id";
    /**
     * Field name of label
     */
    private static final String LABEL = "label";
    /**
     * Field name of properties
     */
    private static final String PROPERTIES = "properties";
    /**
     * Field name of source
     */
    private static final String SOURCE = "source";
    /**
     * Field name of source
     */
    private static final String TARGET = "target";
    /**
     * Field name of timestamp
     */
    private static final String TIMESTAMP = "timestamp";

    public JSONToStreamGramMapper() {
    }

    public StreamObject<StreamVertex, StreamEdge> map(String s) {
        JsonElement root = new JsonParser().parse(s);
        Gson g = new Gson();
        StreamVertex source;
        StreamVertex target;
        source = new StreamVertex(root.getAsJsonObject().get(SOURCE).getAsJsonObject().get(ID).getAsString(),
                root.getAsJsonObject().get(SOURCE).getAsJsonObject().get(LABEL).getAsString(),
                Properties.createFromMap(g.fromJson(
                        root.getAsJsonObject().get(SOURCE).getAsJsonObject().get(PROPERTIES),
                        new TypeToken<HashMap<String, Object>>() {
                        }.getType()
                        )
                )
        );
        target = new StreamVertex(root.getAsJsonObject().get(TARGET).getAsJsonObject().get(ID).getAsString(),
                root.getAsJsonObject().get(TARGET).getAsJsonObject().get(LABEL).getAsString(),
                Properties.createFromMap(g.fromJson(
                        root.getAsJsonObject().get(TARGET).getAsJsonObject().get(PROPERTIES),
                        new TypeToken<HashMap<String, Object>>() {
                        }.getType()
                        )
                )
        );
        StreamEdge edge = new StreamEdge(root.getAsJsonObject().get(ID).getAsString(),
                root.getAsJsonObject().get(TIMESTAMP).getAsLong(),
                root.getAsJsonObject().get(LABEL).getAsString(),
                Properties.createFromMap(g.fromJson(
                        root.getAsJsonObject().get(PROPERTIES),
                        new TypeToken<HashMap<String, Object>>() {
                        }.getType()
                        )
                ), source.getId(), target.getId());
        return new StreamObject<StreamVertex, StreamEdge>(edge, source, target);
    }
}
