package edu.leipzig.streaming.graphs.patternmatching.impl.simulation.dual.functions;

import edu.leipzig.streaming.graphs.patternmatching.model.*;
import org.apache.flink.api.common.functions.FilterFunction;
import org.gradoop.common.model.impl.properties.PropertyValue;
import org.s1ck.gdl.model.comparables.ComparableExpression;
import org.s1ck.gdl.model.comparables.PropertySelector;
import org.s1ck.gdl.model.predicates.Predicate;
import org.s1ck.gdl.model.predicates.expressions.Comparison;

import java.util.ArrayList;
import java.util.Collection;

public class FilterCandidates implements FilterFunction<StreamObject<StreamVertex, StreamEdge>> {
    private boolean filterForVertexOnly;
    private ArrayList<QueryVertex> queryVertices;
    private Collection<StreamObject<QueryVertex, QueryEdge>> queryTriples;

    public FilterCandidates(Collection<StreamObject<QueryVertex, QueryEdge>> queryTriples, boolean vertexOnly, ArrayList<QueryVertex> queryVertices) {
        this.queryTriples = queryTriples;
        this.filterForVertexOnly = vertexOnly;
        this.queryVertices = queryVertices;
    }

    /**
     * The filter function that evaluates the predicate.
     *
     * <p><strong>IMPORTANT:</strong> The system assumes that the function does not
     * modify the elements on which the predicate is applied. Violating this assumption
     * can lead to incorrect results.
     *
     * @param streamObject The value to be filtered.
     * @return True for values that should be retained, false for values to be filtered out.
     * @throws Exception This method may throw exceptions. Throwing an exception will cause the operation
     *                   to fail and may trigger recovery.
     */
    @Override
    public boolean filter(StreamObject<StreamVertex, StreamEdge> streamObject) throws Exception {
        boolean exist;
        if (filterForVertexOnly) {
            return filterVertexOnly(streamObject);
        } else {
            return filterEdgesWithVertices(streamObject);
        }
    }

    private boolean filterEdgesWithVertices(StreamObject<StreamVertex, StreamEdge> streamObject) {
        boolean exist;
        boolean match = false;
        for (StreamObject<QueryVertex, QueryEdge> o : queryTriples) { //TODO: get by label
            exist = streamObject.equals(o);
            // !no need to verify remaining elements
            if (exist) {
                streamObject.getSource().addVariable(o.getSource().getVariable());
                streamObject.getTarget().addVariable(o.getTarget().getVariable());
                streamObject.getEdge().addVariable(o.getEdge().getVariable());
                if (!match && o.getSource().validatePredicate(streamObject.getSource())
                        && o.getTarget().validatePredicate(streamObject.getTarget())
                        && o.getEdge().validatePredicate(streamObject.getEdge())
                        && validateInterVertexPredicate(o, streamObject)) {
                    match = true;
                }
            }
        }
        return match;
    }

    private boolean filterVertexOnly(StreamObject<StreamVertex, StreamEdge> streamObject) {
        for (QueryVertex v : queryVertices) {
            if (v.innerMatch(streamObject.getSource())) {
                streamObject.getSource().addVariable(v.getVariable());
                if (v.hasPredicateSet()) {
                    if (v.validatePredicate(streamObject.getSource())) {
                        return true;
                    }
                }

            }
            if (v.innerMatch(streamObject.getTarget())) {
                streamObject.getTarget().addVariable(v.getVariable());
                if (v.hasPredicateSet()) {
                    if (v.validatePredicate(streamObject.getSource())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean validateInterVertexPredicate(StreamObject<QueryVertex, QueryEdge> o, StreamObject<StreamVertex, StreamEdge> streamObject) {
        boolean result = true;
        for (Predicate p : o.getEdge().getPredicates()) { // only comparisons we have here and with values
            if (result) {
                if (p.getClass() == Comparison.class) {
                    Comparison comparison = (Comparison) p;
                    ComparableExpression[] list = comparison.getComparableExpressions();
                    if (list[0].getClass() == PropertySelector.class && list[1].getClass() == PropertySelector.class) {
                        PropertySelector propertySelector1 = (PropertySelector) list[0];
                        PropertySelector propertySelector2 = (PropertySelector) list[1];
                        StreamVertex vertex1 = null;
                        StreamVertex vertex2 = null;
                        if (streamObject.getSource().hasVariable(propertySelector1.getVariable())) {
                            vertex1 = streamObject.getSource();
                            if (streamObject.getTarget().hasVariable(propertySelector2.getVariable())) {
                                vertex2 = streamObject.getTarget();
                            }
                        } else if (streamObject.getTarget().hasVariable(propertySelector1.getVariable())) {
                            vertex1 = streamObject.getTarget();
                            if (streamObject.getSource().hasVariable(propertySelector2.getVariable())) {
                                vertex2 = streamObject.getSource();
                            }
                        }
                        PropertyValue value1 = null;
                        PropertyValue value2 = null;
                        if (vertex1 != null && vertex2 != null) {
                            value1 = vertex1.getProperties().get(propertySelector1.getPropertyName());
                            value2 = vertex2.getProperties().get(propertySelector2.getPropertyName());
                        }
                        if (value1 != null && value2 != null) { // check here if right
                            switch (comparison.getComparator()) {
                                case EQ:
                                    result = value1.compareTo(value2) == 0;
                                    break;
                                case NEQ:
                                    result = value1.compareTo(value2) != 0;
                                    break;
                                case GT:
                                    result = value1.compareTo(value2) > 0;
                                    break;
                                case LT:
                                    result = value1.compareTo(value2) < 0;
                                    break;
                                case GTE:
                                    result = value1.compareTo(value2) >= 0;
                                    break;
                                case LTE:
                                    result = value1.compareTo(value2) <= 0;
                                    break;
                            }
                        } else {
                            return false; // found null instead of value to compare with
                        }
                    }  // inter vertex
                }
            } else {// last round was false
                return false;
            }
        }
        return true;
    }
}
