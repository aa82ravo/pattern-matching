package edu.leipzig.streaming.graphs.patternmatching.model;

import org.s1ck.gdl.model.predicates.Predicate;

import java.util.Collection;

public interface HasPredicate {
    void addPredicate(Predicate predicate);

    void setProperty(String key, Object value);

    boolean hasPredicateSet();
    Collection<Predicate> getPredicates();
}
