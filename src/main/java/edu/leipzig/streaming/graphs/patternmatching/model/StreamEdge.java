package edu.leipzig.streaming.graphs.patternmatching.model;

import org.gradoop.common.model.impl.properties.Properties;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StreamEdge extends Element {
    private String sourceId;
    private String targetId;


    /**
     * Default constructor is necessary to apply to POJO rules.
     */
    public StreamEdge() {
    }

    /**
     * constructor
     */
    public StreamEdge(String id, String label, Properties properties) {
        super(id, label, properties);
        // this.hash = DigestUtils.sha1Hex(this.toString());
    }

    /**
     * constructor with all fields
     */
    public StreamEdge(String id, Long timestamp, String label, Properties properties, String sourceId, String targetId) {
        super(id, label, properties);
        this.timestamp = timestamp;
        this.sourceId = sourceId;
        this.targetId = targetId;
        // this.hash = DigestUtils.sha1Hex(this.toString());
    }


    public StreamEdge(String id, Long timestamp, String label, Properties properties, String sourceId, String targetId, String variable) {
        super(id, label, properties);
        this.timestamp = timestamp;
        this.sourceId = sourceId;
        this.targetId = targetId;
        this.variables.add(variable);
    }

    public static StreamEdge create(String id, Long timestamp, String label, Properties properties, String sourceId, String targetId, String variable) {
        return new StreamEdge(id, timestamp, label, properties, sourceId, targetId, variable);
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public String toString() {
        return String.format("%s:(%s)-[%s]->(%s)", convertTime(this.timestamp), this.sourceId, super.toString(), this.targetId);
    }

    private String convertTime(long time) {
        Date date = new Date(time);
        Format format = new SimpleDateFormat("yyyy MM dd HH:mm:ss");
        return format.format(date);
    }
}
